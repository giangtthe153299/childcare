/*
 Navicat Premium Data Transfer

 Source Server         : k
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : swp

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 20/05/2021 20:31:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` tinyint(1) NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK`(`role_id`) USING BTREE,
  CONSTRAINT `FK` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES (1, 'aliquam@nibhdolornonummy.ca', 'April', 'Brynne Summers', 1, '0225792896', '992-4609 Vestibulum Av.', '1', 3);
INSERT INTO `account` VALUES (2, 'Fusce@loremipsum.ca', 'Rhona', 'Bo Yates', 0, '0559706307', '3852 Ridiculus Rd.', '2', 4);
INSERT INTO `account` VALUES (3, 'eu.odio@Fusce.com', 'Illana', 'Autumn Fulton', 1, '0919296735', '7654 Elementum St.', '3', 2);
INSERT INTO `account` VALUES (4, 'magna@Nullatempor.co.uk', 'Sigourney', 'Guinevere Blackburn', 1, '0470342636', '4647 Tempor Rd.', '4', 2);
INSERT INTO `account` VALUES (5, 'cursus.vestibulum.Mauris@Aliquamgravida.edu', 'Hermione', 'Debra Young', 1, '0196303756', '3259 Purus. Rd.', '5', 2);
INSERT INTO `account` VALUES (6, 'Curabitur.consequat.lectus@cubiliaCurae.edu', 'MacKensie', 'Elaine Weeks', 1, '0654221692', '1812 Natoque Rd.', '6', 3);
INSERT INTO `account` VALUES (7, 'Cum.sociis.natoque@necmaurisblandit.co.uk', 'Germane', 'Martina Rodgers', 0, '0662288538', '622-1528 Volutpat Av.', '7', 1);
INSERT INTO `account` VALUES (8, 'Sed.auctor.odio@urnajustofaucibus.ca', 'Lacy', 'Halee Leach', 1, '0261195816', 'P.O. Box 944, 7401 Ipsum. St.', '8', 2);
INSERT INTO `account` VALUES (9, 'blandit@dapibusquamquis.co.uk', 'Piper', 'Halla Anthony', 1, '0119475627', '540-9714 Felis Road', '9', 3);
INSERT INTO `account` VALUES (10, 'Integer.aliquam.adipiscing@vel.com', 'Rhonda', 'Linda Solis', 0, '0321248916', '6661 Neque Avenue', '10', 4);
INSERT INTO `account` VALUES (11, 'dolor.dapibus.gravida@Nulla.co.uk', 'Emi', 'Sybill Wagner', 0, '0603228286', '2734 Dui. Street', '11', 4);
INSERT INTO `account` VALUES (12, 'imperdiet.ullamcorper@Nuncmaurissapien.co.uk', 'Aurelia', 'Celeste Barron', 0, '0898733793', '303-4873 Duis Rd.', '12', 4);
INSERT INTO `account` VALUES (13, 'faucibus.id@egestasadui.net', 'Giselle', 'Cally Mckinney', 1, '0933925868', 'Ap #440-7242 Lobortis Road', '13', 3);
INSERT INTO `account` VALUES (14, 'ac.mattis@et.ca', 'Noel', 'Marcia Dodson', 0, '0897036524', 'Ap #476-9416 Morbi Rd.', '14', 4);
INSERT INTO `account` VALUES (15, 'ut.ipsum.ac@Proin.net', 'Mechelle', 'Ruth Glenn', 0, '0115225427', '791-3993 Diam Road', '15', 1);
INSERT INTO `account` VALUES (16, 'mollis.non.cursus@cursus.com', 'Gail', 'Sigourney Keller', 0, '0271628087', '289-5821 Ornare. Street', '16', 2);
INSERT INTO `account` VALUES (17, 'consequat.lectus@ipsum.edu', 'Althea', 'Jade Chang', 0, '0647998539', '410-9038 Semper Rd.', '17', 4);
INSERT INTO `account` VALUES (18, 'primis.in.faucibus@euturpis.co.uk', 'Shoshana', 'Hollee Golden', 1, '0932558198', 'Ap #876-8951 Enim. Rd.', '18', 3);
INSERT INTO `account` VALUES (19, 'nonummy@Maecenas.net', 'Keelie', 'Maxine Mathis', 1, '0608159107', 'Ap #843-1727 Ac St.', '19', 3);
INSERT INTO `account` VALUES (20, 'amet.nulla.Donec@Suspendissetristiqueneque.net', 'Leslie', 'Sybil Fuentes', 1, '0157666445', '129-2060 Adipiscing St.', '20', 1);
INSERT INTO `account` VALUES (21, 'ornare@egetmollislectus.com', 'Galena', 'Eden Grant', 0, '0135037341', 'P.O. Box 624, 8936 In St.', '21', 1);
INSERT INTO `account` VALUES (22, 'tempus.risus.Donec@necante.net', 'Ivory', 'May Copeland', 1, '0837662391', '140-2049 Amet, St.', '22', 1);
INSERT INTO `account` VALUES (23, 'nec.cursus.a@arcuVestibulum.edu', 'Bo', 'Mara Dalton', 0, '0730490877', '146-6971 Elementum, Street', '23', 1);
INSERT INTO `account` VALUES (24, 'dui.Cras.pellentesque@lorem.com', 'Imani', 'Cailin Patel', 1, '0231918392', '7950 Tristique Rd.', '24', 3);
INSERT INTO `account` VALUES (25, 'vitae.diam@dui.ca', 'Eden', 'Kai Shaw', 1, '0721772746', 'P.O. Box 278, 9740 Quisque Street', '25', 3);
INSERT INTO `account` VALUES (26, 'egestas.hendrerit@mauriserat.net', 'Wynne', 'Leilani Davis', 1, '0650286200', '115 Nulla Rd.', '26', 3);
INSERT INTO `account` VALUES (27, 'eu.lacus.Quisque@ligula.ca', 'Dara', 'Keiko Tucker', 0, '0617291317', 'Ap #549-5875 Posuere St.', '27', 1);
INSERT INTO `account` VALUES (28, 'Nam.ac@interdumSed.com', 'Caryn', 'Madeline Mckee', 0, '0915978706', '3222 Nec, Rd.', '28', 2);
INSERT INTO `account` VALUES (29, 'molestie@Proin.edu', 'Lillith', 'Kelsie Hogan', 1, '0655287977', '704-102 Magna. Road', '29', 2);
INSERT INTO `account` VALUES (30, 'et@ultriciesadipiscingenim.edu', 'Angela', 'Meredith Newton', 0, '0436457125', 'Ap #724-7504 Eu St.', '30', 4);

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` tinyint(1) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rated_star` int(255) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `image_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE,
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `role_id` int(255) NOT NULL,
  `url_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `url_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `url_id`(`url_id`) USING BTREE,
  CONSTRAINT `group_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `group_ibfk_3` FOREIGN KEY (`url_id`) REFERENCES `url` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for medicine
-- ----------------------------
DROP TABLE IF EXISTS `medicine`;
CREATE TABLE `medicine`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_date` datetime(0) NULL DEFAULT NULL,
  `featured` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thumnail_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `author_id` int(11) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  INDEX `author_id`(`author_id`) USING BTREE,
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `post_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (1, 'Bruno', 'Suki', '2020-08-19 00:00:00', 'Graiden', '1', 10, 3, 'Mufutau');
INSERT INTO `post` VALUES (2, 'Kyle', 'Kessie', '2022-02-05 00:00:00', 'Kane', '2', 10, 4, 'Theodore');
INSERT INTO `post` VALUES (3, 'Calvin', 'Hanae', '2022-03-21 00:00:00', 'Arden', '3', 15, 2, 'Ashton');
INSERT INTO `post` VALUES (4, 'Rigel', 'Chava', '2021-06-04 00:00:00', 'Rafael', '4', 4, 1, 'Silas');
INSERT INTO `post` VALUES (5, 'Hyatt', 'Shelley', '2020-11-17 00:00:00', 'Hector', '5', 7, 4, 'Channing');
INSERT INTO `post` VALUES (6, 'Kieran', 'Chava', '2020-08-12 00:00:00', 'Tiger', '6', 6, 3, 'Peter');
INSERT INTO `post` VALUES (7, 'Griffin', 'Ruth', '2021-06-18 00:00:00', 'Hedley', '7', 5, 1, 'Ignatius');
INSERT INTO `post` VALUES (8, 'Raphael', 'Jada', '2021-04-12 00:00:00', 'Rudyard', '8', 18, 1, 'Eagan');
INSERT INTO `post` VALUES (9, 'Gabriel', 'Sasha', '2020-09-29 00:00:00', 'Akeem', '9', 27, 4, 'Acton');
INSERT INTO `post` VALUES (10, 'Xanthus', 'Jada', '2021-03-30 00:00:00', 'Oscar', '10', 15, 2, 'Dylan');
INSERT INTO `post` VALUES (11, 'Derek', 'Germaine', '2020-09-14 00:00:00', 'Darius', '11', 22, 4, 'Ryder');
INSERT INTO `post` VALUES (12, 'Armand', 'Hedda', '2020-10-27 00:00:00', 'Grady', '12', 20, 1, 'Harlan');
INSERT INTO `post` VALUES (13, 'Sebastian', 'Hiroko', '2020-06-26 00:00:00', 'Jasper', '13', 21, 2, 'Kaseem');
INSERT INTO `post` VALUES (14, 'Cyrus', 'Kai', '2021-07-05 00:00:00', 'Flynn', '14', 7, 4, 'Noah');
INSERT INTO `post` VALUES (15, 'Gary', 'Alisa', '2021-08-11 00:00:00', 'Bruce', '15', 19, 1, 'Adrian');
INSERT INTO `post` VALUES (16, 'Emery', 'Amena', '2020-12-29 00:00:00', 'Mannix', '16', 16, 1, 'Xenos');
INSERT INTO `post` VALUES (17, 'Eric', 'Nell', '2021-04-01 00:00:00', 'Chaney', '17', 24, 2, 'Yuli');
INSERT INTO `post` VALUES (18, 'Chandler', 'Lacey', '2020-12-22 00:00:00', 'Bruce', '18', 18, 2, 'Scott');
INSERT INTO `post` VALUES (19, 'Ray', 'Chantale', '2022-02-26 00:00:00', 'Cairo', '19', 16, 2, 'Laith');
INSERT INTO `post` VALUES (20, 'Branden', 'Delilah', '2022-01-27 00:00:00', 'Paki', '20', 27, 2, 'Alan');
INSERT INTO `post` VALUES (21, 'Russell', 'Keely', '2022-04-30 00:00:00', 'Quamar', '21', 22, 4, 'Elton');
INSERT INTO `post` VALUES (22, 'Cooper', 'Danielle', '2020-11-03 00:00:00', 'Fletcher', '22', 13, 1, 'Len');
INSERT INTO `post` VALUES (23, 'Kelly', 'Wyoming', '2021-11-25 00:00:00', 'Samson', '23', 26, 4, 'Forrest');
INSERT INTO `post` VALUES (24, 'Prescott', 'Joy', '2021-01-08 00:00:00', 'Nehru', '24', 10, 2, 'Chancellor');
INSERT INTO `post` VALUES (25, 'Keaton', 'Aline', '2022-02-03 00:00:00', 'Honorato', '25', 2, 2, 'Randall');
INSERT INTO `post` VALUES (26, 'Aquila', 'Xantha', '2021-08-20 00:00:00', 'Vernon', '26', 29, 2, 'Brody');
INSERT INTO `post` VALUES (27, 'Vladimir', 'Kirestin', '2020-09-21 00:00:00', 'Samson', '27', 30, 3, 'Levi');
INSERT INTO `post` VALUES (28, 'Hop', 'Mollie', '2022-01-10 00:00:00', 'Zachary', '28', 10, 2, 'Elijah');
INSERT INTO `post` VALUES (29, 'Harlan', 'Risa', '2021-04-12 00:00:00', 'Guy', '29', 1, 2, 'Randall');
INSERT INTO `post` VALUES (30, 'Cyrus', 'Germane', '2021-05-27 00:00:00', 'Troy', '30', 8, 3, 'Salvador');

-- ----------------------------
-- Table structure for post_category
-- ----------------------------
DROP TABLE IF EXISTS `post_category`;
CREATE TABLE `post_category`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post_category
-- ----------------------------
INSERT INTO `post_category` VALUES (1, 'Category_1');
INSERT INTO `post_category` VALUES (2, 'Category_2');
INSERT INTO `post_category` VALUES (3, 'Category_3');
INSERT INTO `post_category` VALUES (4, 'Category_4');
INSERT INTO `post_category` VALUES (5, 'Category_5');

-- ----------------------------
-- Table structure for presription_detail
-- ----------------------------
DROP TABLE IF EXISTS `presription_detail`;
CREATE TABLE `presription_detail`  (
  `presription_id` int(255) NOT NULL,
  `medicine_id` int(255) NOT NULL,
  `quantity` int(255) NULL DEFAULT NULL,
  `dosage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`presription_id`, `medicine_id`) USING BTREE,
  INDEX `medicine_id`(`medicine_id`) USING BTREE,
  INDEX `presription_id`(`presription_id`) USING BTREE,
  CONSTRAINT `presription_detail_ibfk_1` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `reservation_date` datetime(0) NULL DEFAULT NULL,
  `checkup_time` datetime(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `staff_id` int(11) NULL DEFAULT NULL,
  `number_of_person` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  INDEX `staff_id`(`staff_id`) USING BTREE,
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`staff_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reservation_service
-- ----------------------------
DROP TABLE IF EXISTS `reservation_service`;
CREATE TABLE `reservation_service`  (
  `reservation_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `unit_price` float(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`reservation_id`, `service_id`, `prescription_id`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE,
  INDEX `prescription_id`(`prescription_id`) USING BTREE,
  CONSTRAINT `reservation_service_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `reservation_service_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `reservation_service_ibfk_3` FOREIGN KEY (`prescription_id`) REFERENCES `presription_detail` (`presription_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'Admin');
INSERT INTO `role` VALUES (2, 'Manager');
INSERT INTO `role` VALUES (3, 'Staff');
INSERT INTO `role` VALUES (4, 'Customer');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `original_price` float(10, 2) NULL DEFAULT NULL,
  `sale_price` float(10, 2) NULL DEFAULT NULL,
  `thumnail_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  CONSTRAINT `service_ibfk_1` FOREIGN KEY (`id`) REFERENCES `service_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for service_category
-- ----------------------------
DROP TABLE IF EXISTS `service_category`;
CREATE TABLE `service_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `backlink` varchar(0) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for url
-- ----------------------------
DROP TABLE IF EXISTS `url`;
CREATE TABLE `url`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `URL` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
